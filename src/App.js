import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          리액트 배우기
        </a>
        <p>지금은 {process.env.NODE_ENV} 모드입니다.</p>
        <p>
          그리고 서버 환경에 따라 달라지는 DB 연결 Endpoint 는{" "}
          {process.env.REACT_APP_DB_ENDPOINT} 입니다.
        </p>
      </header>
    </div>
  );
}

export default App;
