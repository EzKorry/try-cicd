FROM node:16 as build

ARG REACT_APP_DB_ENDPOINT=http://default-endpoint.com
ENV REACT_APP_DB_ENDPOINT ${REACT_APP_DB_ENDPOINT}

WORKDIR /react
COPY ./package.json .
COPY ./yarn.lock .
RUN yarn

COPY ./public ./public
COPY ./src ./src
RUN yarn build 

FROM node:16

WORKDIR /app
RUN yarn init -y && yarn add express
COPY --from=build /react/build ./build
COPY ./server.js .

EXPOSE 3000

ENTRYPOINT [ "node" ]
CMD [ "server.js" ]
